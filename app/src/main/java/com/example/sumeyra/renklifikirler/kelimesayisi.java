package com.example.sumeyra.renklifikirler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Created by SumeyraMucahitoglu on 14.10.2017.
 */
public class kelimesayisi extends AppCompatActivity {
    String makale = null;
    Context context = this;
    TableLayout tableLayout, tableLayout2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kelimesayisi);
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        tableLayout2 = (TableLayout) findViewById(R.id.tableLayout2);
        TextView baslik1=new TextView(this);
        baslik1.setText("IN ARTICLE WORDS");
        baslik1.setTextColor(Color.BLACK);
        baslik1.setTextSize(18);
        TextView baslik2=new TextView(this);
        baslik2.setText("TURKISH MEANING");
        baslik2.setTextColor(Color.BLACK);
        baslik2.setTextSize(18);
        TableRow r1=new TableRow(this);
        TableRow r2=new TableRow(this);
        r1.addView(baslik1);
        r2.addView(baslik2);
        r1.setBackground(getResources().getDrawable(R.drawable.rowline));
        r2.setBackground(getResources().getDrawable(R.drawable.rowline));
        tableLayout.addView(r1);
        tableLayout2.addView(r2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            makale = bundle.getCharSequence(Intent.EXTRA_HTML_TEXT).toString();
        }

        //String deneme="Lorem Ipsum dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir Lorem Ipsum adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500’lerden beri endüstri standardı sahte metinler olarak kullanılmıştır Beşyüz yıl boyunca varlığını sürdürmekle kalmamış aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır 1960’larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur";
        Map<String, Integer> siralidizi = new HashMap<String, Integer>();
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(makale.split(" ")));
        ArrayList<String> getKey = new ArrayList<String>();
        ArrayList<Integer> getValue = new ArrayList<Integer>();
        HashMap<String, Integer> h = new HashMap<String, Integer>();
        int s = 0;
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    s++;
                }
            }
            if (!h.containsKey(list.get(i))) {
                h.put(list.get(i).toString(), s);
            }
            s = 0;
        }
        Set set = h.entrySet();
        Iterator i = set.iterator();
        int k = 0;
        while (i.hasNext()) {
            Map.Entry map = (Map.Entry) i.next();
            siralidizi.put(map.getKey().toString(), (Integer) map.getValue());

        }
        Map<String, Integer> sortedMap = kelimesayisi.sortByValue(siralidizi);
        int sayac = 0;
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            if (sayac < 5) {
                final TableRow row = new TableRow(this);
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(layoutParams);
                row.setPadding(5, 20, 150, 20);
                row.setBackground(getResources().getDrawable(R.drawable.rowline));
                try {
                    final TextView kelime = new TextView(this);
                    kelime.setGravity(Gravity.LEFT);
                    kelime.setText(entry.getKey());
                    kelime.setTextColor(Color.BLACK);
                    kelime.setTextSize(18);
                    row.addView(kelime);

                    String textToBeTranslated = entry.getKey();
                    String languagePair = "en-tr"; //English to tr ("<source_language>-<target_language>")
                    //Executing the translation function
                    Translate(textToBeTranslated, languagePair);
                    /*final TextView translate = new TextView(this);
                    new TranslateAsyncTask(new TranslateCallback() {

                        @Override
                        public void onSuccess(String result) {
                            translate.setText(result);
                            row.addView(translate);

                        }
                    }).execute(kelime.getText().toString());*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            /*try {
                final TextView translate = new TextView(this);
                //translate.setText(callGoogleTranslateApps(entry.getKey(),"TURKISH","ENGLISH"));

               // translate.setText("" + entry.getValue());
                row.addView(translate);

            } catch (Exception e) {
                e.printStackTrace();
            }*/
                tableLayout.addView(row);
                sayac++;
            } else break;
        }
    }

    void Translate(String textToBeTranslated, String languagePair) {
        TranslatorBackgroundTask translatorBackgroundTask = new TranslatorBackgroundTask(context);
        translatorBackgroundTask.execute(textToBeTranslated, languagePair); // Returns the translated text as a String
        // Log.d("Translation Result",translationResult); // Logs the result in Android Monitor
    }

    public static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }


    class TranslatorBackgroundTask extends AsyncTask<String, Void, String> {
        //Declare Context
        Context ctx;

        //Set Context
        TranslatorBackgroundTask(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            //String variables
            String textToBeTranslated = params[0];
            String languagePair = params[1];

            String jsonString;

            try {
                //Set up the translation call URL
                String yandexKey = "trnsl.1.1.20171015T132342Z.ac4593f9d8057891.6503f95f763156e89231f77f22390885f524991f";
                String yandexUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + yandexKey
                        + "&text=" + textToBeTranslated + "&lang=" + languagePair;
                URL yandexTranslateURL = new URL(yandexUrl);

                //Set Http Conncection, Input Stream, and Buffered Reader
                HttpURLConnection httpJsonConnection = (HttpURLConnection) yandexTranslateURL.openConnection();
                InputStream inputStream = httpJsonConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                //Set string builder and insert retrieved JSON result into it
                StringBuilder jsonStringBuilder = new StringBuilder();
                while ((jsonString = bufferedReader.readLine()) != null) {
                    jsonStringBuilder.append(jsonString + "\n");
                }

                //Close and disconnect
                bufferedReader.close();
                inputStream.close();
                httpJsonConnection.disconnect();

                //Making result human readable
                String resultString = jsonStringBuilder.toString().trim();
                //Getting the characters between [ and ]
                resultString = resultString.substring(resultString.indexOf('[') + 1);
                resultString = resultString.substring(0, resultString.indexOf("]"));
                //Getting the characters between " and "
                resultString = resultString.substring(resultString.indexOf("\"") + 1);
                resultString = resultString.substring(0, resultString.indexOf("\""));

                Log.d("Translation Result:", resultString);
                return resultString;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {

            TextView text = new TextView(kelimesayisi.this);
            text.setText(result);
            text.setTextColor(Color.BLACK);
            text.setTextSize(18);
            text.setGravity(Gravity.LEFT);
            TableRow row2 = new TableRow(kelimesayisi.this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row2.setLayoutParams(layoutParams);
            row2.setPadding(5, 20, 150, 20);
            row2.setBackground(getResources().getDrawable(R.drawable.rowline));
            row2.addView(text);
            tableLayout2.addView(row2);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
