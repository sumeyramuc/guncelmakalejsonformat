package com.example.sumeyra.renklifikirler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by SumeyraMucahitoglu on 13.10.2017.
 */
public class makaleoku extends AppCompatActivity {

    public class URLDrawable extends BitmapDrawable {
        // the drawable that you need to set, you could set the initial drawing
        // with the loading image if you need to
        protected Drawable drawable;

        @Override
        public void draw(Canvas canvas) {
            // override the draw to facilitate refresh function later
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }
    }

    public class URLImageParser implements Html.ImageGetter {
        Context c;
        View container;
        public URLImageParser(View t, Context c) {
            this.c = c;
            this.container = t;
        }

        public Drawable getDrawable(String source) {
            URLDrawable urlDrawable = new URLDrawable();

            // get the actual source
            ImageGetterAsyncTask asyncTask =
                    new ImageGetterAsyncTask(urlDrawable);

            asyncTask.execute(source);

            // return reference to URLDrawable where I will change with actual image from
            // the src tag
            return urlDrawable;
        }

        public class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
            URLDrawable urlDrawable;

            public ImageGetterAsyncTask(URLDrawable d) {
                this.urlDrawable = d;
            }

            @Override
            protected Drawable doInBackground(String... params) {
                String source = params[0];
                return fetchDrawable(source);
            }

            @Override
            protected void onPostExecute(Drawable result) {
                // set the correct bound according to the result from HTTP call
                urlDrawable.setBounds(0, 0, 0 + result.getIntrinsicWidth(), 0
                        + result.getIntrinsicHeight());

                // change the reference of the current drawable to the result
                // from the HTTP call
                urlDrawable.drawable = result;

                // redraw the image by invalidating the container
                URLImageParser.this.container.invalidate();
            }

            /***
             * Get the Drawable from URL
             *
             * @param urlString
             * @return
             */
            public Drawable fetchDrawable(String urlString) {
                try {
                    InputStream is = fetch(urlString);
                    Drawable drawable = Drawable.createFromStream(is, "src");
                    drawable.setBounds(0, 0, 0 + drawable.getIntrinsicWidth(), 0
                            + drawable.getIntrinsicHeight());
                    return drawable;
                } catch (Exception e) {
                    return null;
                }
            }

            private InputStream fetch(String urlString) throws MalformedURLException, IOException {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet request = new HttpGet(urlString);
                HttpResponse response = httpClient.execute(request);
                return response.getEntity().getContent();
            }
        }
    }

    TextView makaleoku;
    private static final int READ_REQUEST_CODE = 42;
    String htmlString = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.makaleoku);
        Button tekrarEdenKelimeler=(Button) findViewById(R.id.tekrarEdenKelimeler);
        tekrarEdenKelimeler.setTransformationMethod(null);
        tekrarEdenKelimeler.getBackground().setColorFilter(Color.parseColor("#A52A2A"), PorterDuff.Mode.DARKEN);
        tekrarEdenKelimeler.setTextColor(Color.WHITE);
        tekrarEdenKelimeler.setTextSize(15);

        this.makaleoku = (TextView) this.findViewById(R.id.makaleoku);
        Intent intent = getIntent();
        htmlString = intent.getStringExtra("stringHtml");
        URLImageParser p = new URLImageParser(makaleoku, this);
        final Spanned htmlSpan = Html.fromHtml(htmlString, p, null);
        makaleoku.setText(htmlSpan);
        makaleoku.setTextSize(18);
        makaleoku.setTextColor(Color.BLACK);
        makaleoku.setMovementMethod(LinkMovementMethod.getInstance()); //makaledeki linkleri aktif eder.

        tekrarEdenKelimeler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(makaleoku.this,kelimesayisi.class);
                intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml(htmlString));
                startActivity(intent);
            }
        });
        }

    }