package com.example.sumeyra.renklifikirler;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.Html;
import android.text.Layout;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
//SumeyraMucahioglu
public class MainActivity extends AppCompatActivity {
    TableLayout tableLayout;
    List<String> dizi= new ArrayList<String>();
    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        TableRow row1 = new TableRow(this);
        TextView title=(TextView)findViewById(R.id.textView);
        title.setTextSize(22);
        title.setText("WIRED.COM ARTICLES");
        title.setTextColor(Color.BLACK);
        title.setGravity(Gravity.CENTER);
        new getData().execute("https://www.wired.com/wp-json/wp/v2/posts?filter[posts_per_page]=5&filter[order_by]='date'&filter[order]=desc");

    }

    class getData extends AsyncTask<String,String,String> {
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                String addline = "";
                while ((line = bufferedReader.readLine()) != null) {
                    addline += line;
                }
                return addline;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hatali islem";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            JSONArray jsonArray = null;
            JSONObject jsonObject = null, subObject = null;
            try {
                jsonArray = new JSONArray(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                final TableRow row = new TableRow(MainActivity.this);
                try {
                    jsonObject = jsonArray.getJSONObject(i);

                    subObject = jsonObject.getJSONObject("title");
                    String title = subObject.getString("rendered");

                    dizi.add(title);
                    final Button butontitle = new Button(MainActivity.this);
                    butontitle.setBackgroundColor(Color.TRANSPARENT);
                    butontitle.setTransformationMethod(null);
                    butontitle.setText(Html.fromHtml(title));
                    butontitle.setTextSize(18);
                    butontitle.setGravity(Gravity.LEFT);

                    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(layoutParams);
                    row.setPadding(2, 20, 2, 20);
                    row.setBackground(getResources().getDrawable(R.drawable.rowline));

                    row.addView(butontitle);
                    final JSONObject jsonObject2=jsonObject;
                    butontitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            butontitle.setBackgroundResource(R.drawable.selector);
                            try {
                                JSONObject subObject2 = jsonObject2.getJSONObject("content");
                                String content = subObject2.getString("rendered");
                                Intent intent = new Intent(MainActivity.this, makaleoku.class);
                                //intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml(content));
                                intent.putExtra("stringHtml", content);
                                intent.setType("text/html");
                                startActivity(Intent.createChooser(intent, null));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                tableLayout.addView(row);
            }
        }
    }
}
